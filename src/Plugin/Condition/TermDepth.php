<?php

/**
 * @file
 * Contains \Drupal\term_depth_condition\Plugin\Condition\TermDepth.
 */

namespace Drupal\term_depth_condition\Plugin\Condition;

use Drupal\Core\Condition\ConditionPluginBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\taxonomy\TermInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a condition based on taxonomy term depth.
 *
 * @Condition(
 *   id = "term_depth",
 *   label = @Translation("Term Depth"),
 *   context_definitions = {
 *     "taxonomy_term" = @ContextDefinition("entity:taxonomy_term", required = FALSE , label = @Translation("Taxonomy term"))
 *   }
 * )
 *
 */
class TermDepth extends ConditionPluginBase implements ContainerFactoryPluginInterface {

  /**
   *  Entity manager instance.
   *
   * @var EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Creates a new ExampleCondition instance.
   *
   * @param EntityTypeManagerInterface $entity_manager
   *   The entity storage.
   * @param array $configuration
   *   The plugin configuration, i.e. an array with configuration values keyed
   *   by configuration option name. The special key 'context' may be used to
   *   initialize the defined contexts by setting it to an array of context
   *   values keyed by context names.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(EntityTypeManagerInterface $entity_manager, array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
  {
    return new static(
      $container->get('entity_type.manager'),
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    // Remove option to negate.
    $form['negate']['#access'] = FALSE;

    // Provide a basic text field to enter the term depth.
    $form['depth'] = array(
      '#type' => 'number',
      '#min' => 0,
      '#max' => 8,
      '#step' => 1,
      '#title' => $this->t('Specify term depth'),
      '#default_value' => isset($this->configuration['depth']) ? $this->configuration['depth'] : '',
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
        'depth' => NULL,
      ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['depth'] = $form_state->getValue('depth');
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * Evaluates the condition and returns TRUE or FALSE accordingly.
   *
   * @return bool
   *   TRUE if the condition has been met, FALSE otherwise.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function evaluate() {
    // Only evaluate if a condition is set
    if ($this->configuration['depth'] === '') {
      return TRUE;
    }

    $entity = $this->getContextValue('taxonomy_term');

    // Not in a term context. Try from the route.
    if (!$entity) {
      $entity = \Drupal::routeMatch()->getParameter('taxonomy_term');
      // If the entity extends EntityInterface, we have the entity we want.
      if (!$entity instanceof TermInterface) {
        return FALSE;
      }
      // All checks failed. Stop.
      if (!$entity) {
        return FALSE;
      }
    }

    if ($entity instanceof TermInterface) {
      // Check term depth within the taxonomy tree.
      $vid = $entity->bundle();
      /** @var \Drupal\taxonomy\TermStorage $term_storage */
      $term_storage = $this->entityTypeManager->getStorage('taxonomy_term');
      $depth = (int) $this->configuration['depth'];
      $tree = $term_storage->loadTree($vid, 0, ($depth + 1));
      foreach ($tree as $term_object) {
        if ($term_object->tid == $entity->id() && $term_object->depth === $depth) {
          return TRUE;
        }
      }
    }

    return FALSE;
  }

  /**
   * Provides a human readable summary of the condition's configuration.
   */
  public function summary() {
    $depth = $this->configuration['depth'];

    return $this->t('The term is at a depth of @depth.', array('@depth' => $depth));
  }

}
